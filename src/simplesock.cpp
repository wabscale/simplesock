#include "simplesock.h"


SimpleSock::SimpleSock(const std::string& address, int port) : io_service(), port(port), address(address) {
    this->socket = new tcp::socket(io_service);
    socket->connect(tcp::endpoint(boost::asio::ip::address::from_string(address), port));
}

SimpleSock::SimpleSock(const SimpleSock& sock) : io_service(), port(sock.port), address(sock.address) {
    this->socket = new tcp::socket(io_service);
    socket->connect(tcp::endpoint(boost::asio::ip::address::from_string(address), port));
}

SimpleSock::~SimpleSock() {
    delete socket;
}

int SimpleSock::send(const std::string& message) {
    boost::asio::write(*socket, boost::asio::buffer(message));
}

int SimpleSock::sendline(const std::string& message) {
    send(message);
    send("\n");
}

std::string SimpleSock::recvline(int lines) {
    boost::asio::streambuf buf;
    //boost::system::error_code error;
    for (int line_count=0; line_count<lines; ++line_count) {
        boost::asio::read_until(*socket, buf, "\n");
    }
    return boost::asio::buffer_cast<const char*>(buf.data());
}

std::string SimpleSock::recvuntil(char c) {}
