#include <iostream>
#include "simplesock.h"

using namespace std;

int main(int argc, char** argv) {
    SimpleSock sock("127.0.0.1", 1234);
    sock.sendline("hello!");
    sock.recvline();
}
